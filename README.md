# Command line instructions


### Git global setup

> git config --global user.name "hdlin"
> git config --global user.email "javack@foxmail.com"

### Create a new repository

> git clone git@gitlab.com:hdlin88/shopnc.git
> cd shopnc
> touch README.md
> git add README.md
> git commit -m "add README"
> git push -u origin master

### Existing folder or Git repository

> cd existing_folder
> git init
> git remote add origin git@gitlab.com:hdlin88/shopnc.git
> git add .
> git commit
> git push -u origin master
